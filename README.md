# TGV

An attempt on a simple transportation system. The idea originated from an attempted pun on TJV (the
subject this project is for) - french high speed trains TGV.

## Structure

- modified from example project built during labs

### Tables/Entities

![diagram](diagram.png)

- NOTE: diagram has cut off the _M:N_ connection between `Station`s

- **Station**:
  - id
  - label: `String`
- **Company**:
  - id
  - name: `String`
- **connection**: many-to-many decomposition table → `/station/{id}/outgoing/{dstId}`
  - from: `Station.id`
  - to: `Station.id`
- **Train**:
  - id
  - at: `Station.id`
  - company: `Company.id`

### Extra requests

- data operation: filtering `Train`s by `Company.name`
- client action: creating a train for company that doesn't exit will create that company first

### Openapi

- generated via `springdocs-openapi-starter`
- can be found at `<serverurl>/openapi.yaml` or `<serverurl>/openapi.json`;
  - `localhost:8080/openapi.yaml` by default
- unfortunately generator messes up response body placement (really illogical why it always ended at
  exceptions)
- available at `./openapi.yaml` in corrected and enhanced form

## Installation

### Defaults

- **client**: `localhost:9080/`
- **server**: `localhost:8080`
- expects:
  - **postgres** on `localhost:5432` with user `postgres`, no password, db called `tgv`
  - java 17

```sh
chmod a+x ./run.sh && ./run.sh
# or for direct modifications (requires bash)
systemctl status postgresql | grep 'Active: active' || systemctl start postgresql
export SPRING_DATASOURCE_USERNAME=postgres
export SPRING_DATASOURCE_PASSWORD=
createdb -U $SPRING_DATASOURCE_USERNAME tgv
psql -U $SPRING_DATASOURCE_USERNAME -d tgv -f create.sql

chmod a+x ./client/gradlew ./server/gradlew
cd server && ./gradlew test bootJar && cd ..
cd client && ./gradlew bootJar && cd ..

export API_URL=http://localhost:8080
SPRING_PORT=${API_URL/*:} java -jar ./server/build/libs/api*.jar & # server

export SPRING_PORT=9080
java -jar ./client/build/libs/client*.jar & # client

sleep 2 && $BROWSER "http://localhost:$SPRING_PORT/"
```

### With docker

- attempt with `docker compose -f ./ci/compose.yaml up -d` I tried, never found out how it works
  anyway
