package cz.plitos.tgv.api;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;

import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;

public abstract class CrudClient<M, ID> {
	private final String baseUrl;
	protected final RestClient client;
	private RestClient dtoClient;
	private ID dtoSelected;

	public CrudClient(String baseUrl) {
		this.baseUrl = baseUrl;
		client = RestClient.create(baseUrl);
	}

	public RestClient client(ID id) {
		return id.equals(dtoSelected) ? dtoClient :
			(dtoClient = RestClient.builder().baseUrl(baseUrl + "/{id}")
				.defaultUriVariables(Map.of("id", dtoSelected = id)).build());
	}

	protected RestClient client(String path, Map<String, ?> uriVars) {
		return RestClient.builder().baseUrl(baseUrl + path).defaultUriVariables(uriVars).build();
	}

	private M getDto(RestClient.ResponseSpec response) {
		try {
			return response.body((Class<M>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0]);
		} catch (HttpClientErrorException e) {
			throw e;
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	private Collection<M> getList(RestClient.ResponseSpec response) {
		try {
			return Arrays.asList((M[]) response.body(Array.newInstance(
				(Class) ((ParameterizedType) this.getClass().getGenericSuperclass())
					.getActualTypeArguments()[0], 0).getClass()));
		} catch (HttpClientErrorException e) {
			throw e;
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}


	public M readById(ID id) {
		try {
			return getDto(client(id).get().retrieve());
		} catch (HttpClientErrorException.NotFound e) {
			throw new NoSuchElementException();
		}
	}

	public Collection<M> readAll() {
		return getList(client.get().retrieve());
	}

	public M create(M data) {
		return getDto(client.post().body(data).retrieve());
	}

	public void update(ID id, M data) {
		try {
			client(id).put().body(data).retrieve().toBodilessEntity();
		} catch (HttpClientErrorException.NotFound e) {
			throw new NoSuchElementException();
		}
	}

	public void delete(ID id) {
		try {
			client(id).delete().retrieve().toBodilessEntity();
		} catch (HttpClientErrorException.NotFound e) {
			throw new NoSuchElementException();
		} catch (HttpClientErrorException.Conflict e) {
			throw new IllegalArgumentException("ID " + id + " is being referred to.");
		}
	}
}
