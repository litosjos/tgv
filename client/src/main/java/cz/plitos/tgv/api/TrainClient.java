package cz.plitos.tgv.api;

import cz.plitos.tgv.model.TrainDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;

import java.util.*;

@Component
public class TrainClient extends CrudClient<TrainDto, Long> {
	public TrainClient(@Value("${api.url}/train") String baseUrl) {
		super(baseUrl);
	}

	public Collection<TrainDto> readByCompanyOrStation(Optional<String> company,
																										 Optional<String> station) {
		Map<String, String> params = new HashMap<>();
		StringBuilder path = new StringBuilder("?");
		if (company.isPresent()) {
			params.put("company", company.get());
			path.append("company={company}");
		}
		if (station.isPresent()) {
			params.put("station", station.get());
			if (company.isPresent()) path.append('&');
			path.append("station={station}");
		}
		if (params.isEmpty()) return readAll();
		return Arrays.asList(client(path.toString(), params).get().retrieve().body(TrainDto[].class));
	}
}
