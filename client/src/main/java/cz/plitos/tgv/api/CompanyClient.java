package cz.plitos.tgv.api;

import cz.plitos.tgv.model.CompanyDto;
import cz.plitos.tgv.model.StationDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;

import java.util.*;

@Component
public class CompanyClient extends CrudClient<CompanyDto, Long> {
	public CompanyClient(@Value("${api.url}/company") String baseUrl) {
		super(baseUrl);
	}

	public CompanyDto readByName(String name) {
		try {
			return client("?name={name}", Map.of("name", name)).get().retrieve().body(CompanyDto[].class)[0];
		} catch (HttpClientErrorException.NotFound e) {
			throw new NoSuchElementException();
		}
	}
}
