package cz.plitos.tgv.api;

import cz.plitos.tgv.model.StationDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;

import java.util.*;

@Component
public class StationClient extends CrudClient<StationDto, Long> {
	public StationClient(@Value("${api.url}/station") String baseUrl) {
		super(baseUrl);
	}

	public Collection<StationDto> readOutgoing(Long id) {
		try {
			return Arrays.asList(client("/{id}/outgoing", Map.of("id", id)).get().retrieve().body(StationDto[].class));
		} catch (HttpClientErrorException.NotFound e) {
			throw new NoSuchElementException();
		}
	}

	public void delOutgoing(Long id, Long dstId) {
		try {
			client("/{id}/outgoing/{dst}", Map.of("id", id, "dst", dstId))
				.delete().retrieve().toBodilessEntity();
		} catch (HttpClientErrorException.NotFound e) {
			throw new NoSuchElementException();
		}
	}

	public void addOutgoing(Long id, Long dstId) {
		try {
			client("/{id}/outgoing", Map.of("id", id)).post()
				.contentType(MediaType.APPLICATION_JSON).body(dstId).retrieve().toBodilessEntity();
		} catch (HttpClientErrorException.NotFound | HttpClientErrorException.Conflict e) {
			throw new NoSuchElementException();
		}
	}

	public StationDto readByLabel(String label) {
		try {
			return client("?label={label}", Map.of("label", label)).get().retrieve().body(StationDto[].class)[0];
		} catch (HttpClientErrorException.NotFound e) {
			throw new NoSuchElementException();
		}
	}
}
