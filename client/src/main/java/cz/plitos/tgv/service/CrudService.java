package cz.plitos.tgv.service;

import cz.plitos.tgv.api.CrudClient;
import cz.plitos.tgv.model.IdDto;

import java.util.Collection;

public class CrudService<M extends IdDto<ID>, ID, C extends CrudClient<M, ID>> {
	protected final C client;

	CrudService(C client) {
		this.client = client;
	}

	public M readById(ID id) {
		return client.readById(id);
	}

	public Collection<M> readAll() {
		return client.readAll();
	}

	public M create(M data) {
		return client.create(data);
	}

	public void update(M data) {
		client.update(data.getId(), data);
	}

	public void delete(ID id) {
		client.delete(id);
	}
}
