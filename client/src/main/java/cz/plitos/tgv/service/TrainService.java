package cz.plitos.tgv.service;

import cz.plitos.tgv.api.CompanyClient;
import cz.plitos.tgv.api.StationClient;
import cz.plitos.tgv.api.TrainClient;
import cz.plitos.tgv.model.TrainDto;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class TrainService extends CrudService<TrainDto, Long, TrainClient> {
	private final CompanyClient cc;
	private final StationClient sc;

	public TrainService(TrainClient client, CompanyClient cc, StationClient sc) {
		super(client);
		this.cc = cc;
		this.sc = sc;
	}

	public Collection<TrainDto> readAllOrByCompanyOrStation(Optional<String> company,
																													Optional<String> station) {
		return client.readByCompanyOrStation(company, station);
	}

	private TrainDto ensureValid(TrainDto data) {
		try {
			data.setCompany(cc.readByName(data.getCompany().getName()));
		} catch (NoSuchElementException e) {
			data.setCompany(cc.create(data.getCompany())); // create new
		}

		if (data.getAt() != null && !data.getAt().getLabel().isEmpty()) { // station is optional
			try {
				data.setAt(sc.readByLabel(data.getAt().getLabel()));
			} catch (NoSuchElementException e) {
				data.setAt(sc.create(data.getAt())); // create new
			}
		} else data.setAt(null);
		return data;
	}

	@Override
	public TrainDto create(TrainDto data) {
		return client.create(ensureValid(data));
	}

	@Override
	public void update(TrainDto data) {
		client.update(data.getId(), ensureValid(data));
	}
}
