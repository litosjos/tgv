package cz.plitos.tgv.service;

import cz.plitos.tgv.api.StationClient;
import cz.plitos.tgv.model.CompanyDto;
import cz.plitos.tgv.model.StationDto;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class StationService extends CrudService<StationDto, Long, StationClient> {
	public StationService(StationClient client) {
		super(client);
	}

	public Collection<StationDto> readOutgoing(Long id) {
		return client.readOutgoing(id);
	}

	public void delOutgoing(Long id, Long dstId) {
		client.delOutgoing(id, dstId);
	}

	public void addOutgoing(Long id, Long dstId) {
		client.addOutgoing(id, dstId);
	}

	public StationDto readByLabel(String label) {
		return client.readByLabel(label);
	}
}
