package cz.plitos.tgv.service;

import cz.plitos.tgv.api.CompanyClient;
import cz.plitos.tgv.model.CompanyDto;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CompanyService extends CrudService<CompanyDto, Long, CompanyClient> {
	public CompanyService(CompanyClient client) {
		super(client);
	}

	public CompanyDto readByName(String name) {
		return client.readByName(name);
	}
}
