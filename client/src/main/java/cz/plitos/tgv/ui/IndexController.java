package cz.plitos.tgv.ui;

import cz.plitos.tgv.service.CompanyService;
import cz.plitos.tgv.service.StationService;
import cz.plitos.tgv.service.TrainService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

	private final CompanyService cc;
	private final StationService sc;
	private final TrainService tc;

	IndexController(CompanyService cc, StationService sc, TrainService tc) {
		this.cc = cc;
		this.sc = sc;
		this.tc = tc;
	}

	@GetMapping("/")
	public String showIndex(Model model) {
		model.addAttribute("companies", cc.readAll());
		model.addAttribute("stations", sc.readAll());
		model.addAttribute("trains", tc.readAll());
		return "index";
	}
}
