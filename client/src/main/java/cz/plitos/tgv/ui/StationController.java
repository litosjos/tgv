package cz.plitos.tgv.ui;

import cz.plitos.tgv.model.StationDto;
import cz.plitos.tgv.service.StationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@Controller
@RequestMapping("/station")
public class StationController extends CrudController<StationService, StationDto, Long> {
	public StationController(StationService service) {
		super(service);
	}

	@GetMapping
	public String index(Model model) {
		return super.index(model);
	}

	@GetMapping("/{id}")
	public String detail(@PathVariable Long id, Model model) {
		String ret = super.detail(id, model);
		model.addAttribute("outgoing", service.readOutgoing(id));
		model.addAttribute("delPath", "");
		return ret;
	}

	@GetMapping("/{id}/edit")
	public String showEdit(@PathVariable Long id, Model model) {
		String ret = super.showEdit(id, model);
		model.addAttribute("outgoing", service.readOutgoing(id));
		return ret;
	}


	@PostMapping("/{id}/outgoing")
	public String connect(@PathVariable Long id, @RequestParam String label, Model model) {
		try {
			Long dst = service.readByLabel(label).getId();
			if (dst.equals(id)) {
				model.addAttribute("error", "Station cannot connect to itself");
				return detail(id, model);
			}
			service.addOutgoing(id, dst);
		} catch (NoSuchElementException e) {
			model.addAttribute("error", "Station called '" + label + "' doesn't exist.");
			return detail(id, model);
		}
		return "redirect:/" + name + "/" + id;
	}

	@DeleteMapping("/{id}/outgoing/{dst}")
	public String disconnect(@PathVariable Long id, @PathVariable Long dst, Model model) {
		try {
			service.delOutgoing(id, dst);
		} catch (NoSuchElementException e) {
			return index(error(model, "disconnect", id));
		}
		return "redirect:/" + name + "/" + id;
	}
}