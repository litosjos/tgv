package cz.plitos.tgv.ui;

import cz.plitos.tgv.model.IdDto;
import cz.plitos.tgv.service.CrudService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.NoSuchElementException;

public class CrudController<S extends CrudService<M, ID, ?>, M extends IdDto<ID>, ID> {
	protected final S service;
	protected final String name;

	public CrudController(S service) {
		this.service = service;
		name = this.getClass().getSimpleName().replace("Controller", "").toLowerCase();
	}

	protected String index(Model model) {
		Collection<M> list = service.readAll();
		model.addAttribute("list", list);
		create(model);
		return name + "/index"; // should contain basically the /create page within
	}

	protected Model error(Model model, String method, ID id) {
		model.addAttribute("error", "Cannot " + method + " " + name + ": id " + id + " not found.");
		return model;
	}

	@GetMapping("/{id}")
	public String detail(@PathVariable ID id, Model model) {
		try {
			model.addAttribute("data", service.readById(id));
			return name + "/detail";
		} catch (NoSuchElementException e) {
			return index(error(model, "get detailed view of", id));
		}
	}

	@GetMapping("/create")
	public String create(Model model) {
		try {
			model.addAttribute("data",
				((Class) ((ParameterizedType) this.getClass().getGenericSuperclass())
					.getActualTypeArguments()[1]).getConstructor().newInstance());
		} catch (Exception e) { // should never happen
			model.addAttribute("error",
				"Failed to generate template for creating " + name + ": " + e.getMessage());
			return index(model);
		}
		return name + "/edit";
	}

	@PostMapping
	public String create(@ModelAttribute M data, Model model) {
		try {
			service.create(data).toString();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			model.addAttribute("error", "Failed to create " + name + ": " + e.getMessage());
			return index(model);
		}
		return "redirect:/" + name;
	}

	@DeleteMapping("/{id}")
	public String delete(@PathVariable ID id, Model model) {
		try {
			service.delete(id);
		} catch (NoSuchElementException e) {
			return index(error(model, "delete", id));
		} catch (IllegalArgumentException e) {
			model.addAttribute("error", "Failed to delete " + name + ": " + e.getMessage());
			return index(model);
		}
		return "redirect:/" + name;
	}

	@GetMapping("/{id}/edit")
	public String showEdit(@PathVariable ID id, Model model) {
		try {
			model.addAttribute("data", service.readById(id));
		} catch (NoSuchElementException e) {
			return index(error(model, "edit", id));
		}
		return name + "/edit";
	}

	@PutMapping("/{id}")
	public String submitEdit(@PathVariable ID id, @ModelAttribute M data, Model model) {
		data.setId(id);
		try {
			service.update(data);
		} catch (NoSuchElementException e) {
			index(error(model, "edit", id));
		}
		return "redirect:/" + name;
	}
}
