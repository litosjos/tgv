package cz.plitos.tgv.ui;

import cz.plitos.tgv.model.CompanyDto;
import cz.plitos.tgv.service.CompanyService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Collection;

@Controller
@RequestMapping("/company")
public class CompanyController extends CrudController<CompanyService, CompanyDto, Long> {
	public CompanyController(CompanyService service) {
		super(service);
	}

	@GetMapping
	public String index(Model model) {
		return super.index(model);
	}
}