package cz.plitos.tgv.ui;

import cz.plitos.tgv.model.TrainDto;
import cz.plitos.tgv.service.TrainService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.Optional;

@Controller
@RequestMapping("/train")
public class TrainController extends CrudController<TrainService, TrainDto, Long> {
	public TrainController(TrainService service) {
		super(service);
	}

	@GetMapping
	public String index(@RequestParam Optional<String> company,
											@RequestParam Optional<String> station,
											Model model) {
		if (company.isPresent() && company.get().isEmpty()) company = Optional.empty();
		if (station.isPresent() && station.get().isEmpty()) station = Optional.empty();

		Collection<TrainDto> list = service.readAllOrByCompanyOrStation(company, station);
		model.addAttribute("list", list);
		if (company.isPresent()) model.addAttribute("company", company.get());
		if (station.isPresent()) model.addAttribute("station", station.get());
		create(model);
		return name + "/index";
	}
}