package cz.plitos.tgv.model;

public class CompanyDto extends IdDto<Long> {
	private String name;

	public CompanyDto() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
