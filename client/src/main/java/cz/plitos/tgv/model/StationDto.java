package cz.plitos.tgv.model;

public class StationDto extends IdDto<Long> {
	private String label;

	public StationDto() {
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
