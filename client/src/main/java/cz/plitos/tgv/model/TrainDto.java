package cz.plitos.tgv.model;

public class TrainDto extends IdDto<Long> {
	private StationDto at;
	private CompanyDto company;

	public TrainDto() {
	}

	public StationDto getAt() {
		return at;
	}

	public void setAt(StationDto at) {
		this.at = at;
	}

	public CompanyDto getCompany() {
		return company;
	}

	public void setCompany(CompanyDto company) {
		this.company = company;
	}
}
