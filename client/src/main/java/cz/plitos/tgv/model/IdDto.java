package cz.plitos.tgv.model;

public class IdDto<ID> {
	private ID id;

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return this.getClass() + "{id=" + id + "}";
	}
}
