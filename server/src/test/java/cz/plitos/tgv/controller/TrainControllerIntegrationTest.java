package cz.plitos.tgv.controller;

import cz.plitos.tgv_api.TGVServer;
import cz.plitos.tgv_api.controller.TrainController;
import cz.plitos.tgv_api.domain.Company;
import cz.plitos.tgv_api.domain.Station;
import cz.plitos.tgv_api.domain.Train;
import cz.plitos.tgv_api.repository.CompanyRepository;
import cz.plitos.tgv_api.repository.StationRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest(classes = TGVServer.class)
public class TrainControllerIntegrationTest {
	@Autowired
	private CompanyRepository cr;
	@Autowired
	private StationRepository sr;
	@Autowired
	private TrainController controller;
	Train t1, t2;
	Company c1, c2;
	Station s1;

	@BeforeEach
	void setUp() {
		c1 = new Company();
		c1.setId(1L);
		c1.setName("C1");
		cr.save(c1);

		c2 = new Company();
		c2.setId(2L);
		c2.setName("C2");
		cr.save(c2);

		s1 = new Station();
		s1.setId(1L);
		s1.setLabel("S1");
		sr.save(s1);

		t1 = new Train();
		t1.setCompany(c1);
		t1.setAt(Optional.of(s1));
		t1 = controller.create(t1);

		t2 = new Train();
		t2.setCompany(c2);
		t2.setAt(Optional.of(s1));
		t2 = controller.create(t2);
	}

	@AfterEach
	void cleanUp() {
		controller.readAllOrByCompanyOrStation(Optional.empty(), Optional.empty()).forEach(
			(e) -> controller.deleteById(e.getId()));

		cr.deleteAll();
		sr.deleteAll();
	}

	@Test
	void readAll() {
		var all = controller.readAllOrByCompanyOrStation(
			Optional.of(c1.getName()), Optional.empty()).iterator();
		Assertions.assertEquals(t1, all.next());
		Assertions.assertFalse(all.hasNext()); // verify all are returned
	}

	@Test
	void readByCompany() {
		var all = controller.readAllOrByCompanyOrStation( // verify selection is returned
			Optional.empty(), Optional.of(s1.getLabel())).iterator();
		var got = all.next();
		Assertions.assertTrue(t1.equals(got) || t2.equals(got));
		got = all.next();
		Assertions.assertTrue(t1.equals(got) || t2.equals(got));
		Assertions.assertFalse(all.hasNext());
		t2.setCompany(c1);
		controller.update(t2.getId(), t2);

		all = controller.readAllOrByCompanyOrStation( // verify updated selection is returned
			Optional.of(c1.getName()), Optional.of(s1.getLabel())).iterator();
		got = all.next();
		Assertions.assertTrue(t1.equals(got) || t2.equals(got));
		got = all.next();
		Assertions.assertTrue(t1.equals(got) || t2.equals(got));
		Assertions.assertFalse(all.hasNext());
	}

	@Test
	void readByStation() {
		t1.setAt(Optional.empty());
		controller.update(t1.getId(), t1);
		var all = controller.readAllOrByCompanyOrStation( // verify station filtering
			Optional.empty(), Optional.of(s1.getLabel())).iterator();
		var got = all.next();
    Assertions.assertEquals(t2, got);
		Assertions.assertFalse(all.hasNext());
	}

	@Test
	void readAllOrFilterNone() { // verify nothing is read when filter doesn't match anything
		var all = controller.readAllOrByCompanyOrStation(Optional.of(""), Optional.empty()).iterator();
		Assertions.assertFalse(all.hasNext());
	}
}
