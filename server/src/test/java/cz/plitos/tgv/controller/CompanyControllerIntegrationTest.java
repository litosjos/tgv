package cz.plitos.tgv.controller;

import cz.plitos.tgv_api.TGVServer;
import cz.plitos.tgv_api.controller.CompanyController;
import cz.plitos.tgv_api.domain.Company;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest(classes = TGVServer.class)
public class CompanyControllerIntegrationTest {
	@Autowired
	CompanyController controller;

	@Test
	void readAllContainingName() {
		Company c1 = new Company();
		c1.setName("Search");
		c1 = controller.create(c1);
		Company c2 = new Company();
		c2.setName("Not Searched");
		c2 = controller.create(c2);

		var got = controller.readAll(Optional.of("Search")).iterator();
		Assertions.assertEquals(c1, got.next());
		Assertions.assertFalse(got.hasNext());

		controller.deleteById(c1.getId());
		controller.deleteById(c2.getId());
	}
}
