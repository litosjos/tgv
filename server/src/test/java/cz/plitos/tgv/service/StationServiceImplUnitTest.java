package cz.plitos.tgv.service;

import cz.plitos.tgv_api.TGVServer;
import cz.plitos.tgv_api.domain.Station;
import cz.plitos.tgv_api.repository.StationRepository;
import cz.plitos.tgv_api.service.IdUsedException;
import cz.plitos.tgv_api.service.StationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Optional;

@SpringBootTest(classes = TGVServer.class)
class StationServiceImplUnitTest {
	@Autowired
	private StationServiceImpl service;
	@MockBean
	private StationRepository repo;
	Station s1, s2;

	@BeforeEach
	void setUp() {
		s1 = new Station();
		s1.setId(1L);
		s1.setLabel("S1");
		s1.setOutgoing(new HashSet<>());
		s2 = new Station();
		s2.setId(2L);
		s2.setLabel("S2");
		s2.setIncoming(new HashSet<>());
		Mockito.when(repo.findById(s1.getId())).thenReturn(Optional.of(s1));
		Mockito.when(repo.findById(s2.getId())).thenReturn(Optional.of(s2));
	}

	@Test
	void connectSelfInvalid() {
		Assertions.assertThrows(IdUsedException.class, () -> service.connect(s1.getId(), s1.getId()));

		Assertions.assertTrue(s1.getOutgoing().isEmpty());
		Assertions.assertTrue(s2.getIncoming().isEmpty());
		Mockito.verify(repo, Mockito.never()).save(Mockito.any());
	}

	@Test
	void connectNewIdInvalid() {
		Mockito.when(repo.findById(s2.getId())).thenReturn(Optional.empty());

		Assertions.assertThrows(NoSuchElementException.class, () -> service.connect(s1.getId(), s2.getId()));

		Mockito.verify(repo, Mockito.never()).save(Mockito.any());
	}

	@Test
	void connect() {
		service.connect(s1.getId(), s2.getId());

		Assertions.assertTrue(s1.getOutgoing().contains(s2));
		Assertions.assertEquals(1, s1.getOutgoing().size());
		Assertions.assertTrue(s2.getIncoming().contains(s1));
		Assertions.assertEquals(1, s2.getIncoming().size());
		Mockito.verify(repo, Mockito.atLeastOnce()).save(s1);
		Mockito.verify(repo, Mockito.atLeastOnce()).save(s2);
	}
}