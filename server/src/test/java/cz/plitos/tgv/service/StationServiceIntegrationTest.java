package cz.plitos.tgv.service;

import cz.plitos.tgv_api.TGVServer;
import cz.plitos.tgv_api.domain.Station;
import cz.plitos.tgv_api.service.StationService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.NoSuchElementException;

@SpringBootTest(classes = TGVServer.class)
class StationServiceIntegrationTest {
	@Autowired
	private StationService service;
	Station s1, s2;

	@BeforeEach
	void setUp() {
		s1 = new Station();
		s1.setLabel("S1");
		s1.setIncoming(new HashSet<>());
		s1.setOutgoing(new HashSet<>());
		s1 = service.create(s1);

		s2 = new Station();
		s2.setLabel("S2");
		s2.setOutgoing(new HashSet<>());
		s2.setIncoming(new HashSet<>());
		s2 = service.create(s2);
	}

	@AfterEach
	void cleanUp() {
		service.readAll().forEach((e) -> service.deleteById(e.getId()));
	}

	@Transactional // to access outgoing/incoming - without changes → could be avoided (IDK how)
	@Test
	void connect() {
		service.connect(s1.getId(), s2.getId());

		Station fromDb1 = service.readById(s1.getId()).get(), fromDb2 = service.readById(s2.getId()).get();
		Assertions.assertTrue(fromDb1.getOutgoing().contains(fromDb2));
		Assertions.assertTrue(fromDb2.getIncoming().contains(fromDb1));
		Assertions.assertEquals(1, fromDb1.getOutgoing().size());
		Assertions.assertEquals(1, fromDb2.getIncoming().size());

		disconnect(); // using already created connection
	}

	void disconnect() {
		service.disconnect(s1.getId(), s2.getId());

		Station fromDb1 = service.readById(s1.getId()).get(), fromDb2 = service.readById(s2.getId()).get();
		Assertions.assertEquals(0, fromDb1.getOutgoing().size());
		Assertions.assertEquals(0, fromDb2.getIncoming().size());
	}

	@Test
	void disAndConnectInvalid() {
		try {
			service.connect(s1.getId() + 100L, s2.getId() + 100L);
			Assertions.fail();
		} catch (NoSuchElementException e) {
		}
		try {
			service.disconnect(s1.getId() + 100L, s2.getId() + 100L);
			Assertions.fail();
		} catch (NoSuchElementException e) {
		}
	}
}