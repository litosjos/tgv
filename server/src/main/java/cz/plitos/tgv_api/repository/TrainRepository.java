package cz.plitos.tgv_api.repository;

import cz.plitos.tgv_api.domain.Train;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TrainRepository extends CrudRepository<Train, Long> {
	public Collection<Train> findByAtLabel(String stationLabel);

	public Collection<Train> findByCompanyName(String companyName);

	// Unnecessary but used as an example use of JPQL
	@Query("SELECT t FROM Train t WHERE t.company.name LIKE :company AND t.at.label LIKE :station")
	public Collection<Train> findByCompanyNameAndAtLabel(String company, String station);
}
