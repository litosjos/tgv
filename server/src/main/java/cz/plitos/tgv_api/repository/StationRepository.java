package cz.plitos.tgv_api.repository;

import cz.plitos.tgv_api.domain.Station;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface StationRepository extends CrudRepository<Station, Long> {
	public Optional<Station> findByLabel(String label);
}
