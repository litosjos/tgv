package cz.plitos.tgv_api.domain;

import jakarta.persistence.*;

import java.util.Collection;

@Entity
@Table(name = "company")
public class Company extends EntityWithId<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

//	@OneToMany(mappedBy = "company")
//	private Collection<Train> inventory;

	public Company() {
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Company{" +
			"id=" + id +
			", name='" + name + '\'' +
			'}';
	}
}
