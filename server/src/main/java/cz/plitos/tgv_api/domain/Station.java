package cz.plitos.tgv_api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;

@Entity
@Table(name = "station")
public class Station extends EntityWithId<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String label;

	@ManyToMany
	@JoinTable(name = "connection", joinColumns = @JoinColumn(name = "incoming"), inverseJoinColumns = @JoinColumn(name = "outgoing"))
	@JsonIgnore // stations can be connected in a cycle → cannot transform to JSON
	private Collection<Station> outgoing;

	@ManyToMany(mappedBy = "outgoing")
	@JsonIgnore
	private Collection<Station> incoming;

	public Station() {
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Collection<Station> getOutgoing() {
		return outgoing;
	}

	public void setOutgoing(Collection<Station> outgoing) {
		this.outgoing = outgoing;
	}

	public Collection<Station> getIncoming() {
		return incoming;
	}

	public void setIncoming(Collection<Station> incoming) {
		this.incoming = incoming;
	}

	@Override
	public String toString() {
		return "Station{" +
			"id=" + id +
			", label='" + label + '\'' +
			'}';
	}
}
