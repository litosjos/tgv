package cz.plitos.tgv_api.domain;

import java.util.Objects;

public abstract class EntityWithId<ID> {
	public abstract ID getId();

	public abstract void setId(ID id);

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		return getId().equals(((EntityWithId<ID>) o).getId());
	}

	@Override
	public int hashCode() {
		return getId() == null ? 0 : getId().hashCode();
	}
}
