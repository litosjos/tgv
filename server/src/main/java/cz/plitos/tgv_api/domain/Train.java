package cz.plitos.tgv_api.domain;

import jakarta.persistence.*;

import java.util.Optional;

@Entity
@Table(name = "train")
public class Train extends EntityWithId<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(optional = false)
	private Company company;
	@ManyToOne
	private Station at;

	public Train() {
	}

	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Optional<Station> getAt() {
		if (at == null || at.getLabel() == null) return Optional.empty();
		return Optional.of(at);
	}

	public void setAt(Optional<Station> at) {
		this.at = at.orElse(null);
	}

	@Override
	public String toString() {
		return "Train{" +
			"id=" + id +
			", company=" + company +
			", at=" + at +
			'}';
	}
}
