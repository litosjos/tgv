package cz.plitos.tgv_api.service;

import cz.plitos.tgv_api.domain.Company;

import java.util.Optional;

public interface CompanyService extends CrudService<Company, Long> {
	public Optional<Company> readByName(String name);
}
