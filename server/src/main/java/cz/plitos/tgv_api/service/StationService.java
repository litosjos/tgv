package cz.plitos.tgv_api.service;

import cz.plitos.tgv_api.domain.Station;

import java.util.Optional;

public interface StationService extends CrudService<Station, Long> {
	public Iterable<Station> readOutgoing(long srcId);

	public void connect(long srcId, long dstId) throws IdUsedException;

	public void disconnect(long srcId, long dstId);

	public Optional<Station> readByLabel(String label);
}
