package cz.plitos.tgv_api.service;

import cz.plitos.tgv_api.domain.Train;
import cz.plitos.tgv_api.repository.TrainRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class TrainServiceImpl extends CrudServiceImpl<Train, Long, TrainRepository> implements TrainService {
	public TrainServiceImpl(TrainRepository r) {
		super(r);
	}

	@Override
	public Collection<Train> readByCompanyOrStation(Optional<String> company,
																									Optional<String> station) {
		if (company.isEmpty()) return repo.findByAtLabel(station.get());
		if (station.isEmpty()) return repo.findByCompanyName(company.get());
		return repo.findByCompanyNameAndAtLabel(company.get(), station.get());
	}
}
