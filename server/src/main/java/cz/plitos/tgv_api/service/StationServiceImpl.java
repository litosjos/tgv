package cz.plitos.tgv_api.service;

import cz.plitos.tgv_api.domain.Station;
import cz.plitos.tgv_api.repository.StationRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class StationServiceImpl extends CrudServiceImpl<Station, Long, StationRepository> implements StationService {
	public StationServiceImpl(StationRepository r) {
		super(r);
	}

	@Transactional // otherwise getOutgoing fails because of lazy-loading
	public Iterable<Station> readOutgoing(long srcId) {
		Optional<Station> optSrc = repo.findById(srcId);
		if (optSrc.isEmpty()) throw new NoSuchElementException();

		return optSrc.get().getOutgoing();
	}

	@Transactional
	@Override
	public void connect(long srcId, long dstId) throws IdUsedException {
		if (srcId == dstId) throw new IdUsedException();
		Optional<Station> optSrc = repo.findById(srcId), optDst = repo.findById(dstId);

		if (optSrc.isEmpty() || optDst.isEmpty()) throw new NoSuchElementException();

		Station src = optSrc.get(), dst = optDst.get();

		src.getOutgoing().add(dst);
		dst.getIncoming().add(src);

		repo.save(src);
		repo.save(dst);
	}

	@Transactional
	@Override
	public void disconnect(long srcId, long dstId) {
		if (srcId == dstId) return; // direct loop cannot occur in DB
		Optional<Station> optSrc = repo.findById(srcId), optDst = repo.findById(dstId);

		if (optSrc.isEmpty() || optDst.isEmpty()) throw new NoSuchElementException();

		Station src = optSrc.get(), dst = optDst.get();

		if (!src.getOutgoing().remove(dst)) throw new NoSuchElementException();
		dst.getIncoming().remove(src);

		repo.save(src);
		repo.save(dst);
	}

	public void update(Station e) {
		Station old = repo.findById(e.getId()).orElse(null);
		if (old == null) throw new NoSuchElementException();
		old.setLabel(e.getLabel()); // ignore outgoing/incoming changes - handled by -connect methods
		repo.save(old);
	}

	public Optional<Station> readByLabel(String label) {
		return repo.findByLabel(label);
	}
}
