package cz.plitos.tgv_api.service;

import cz.plitos.tgv_api.domain.Company;
import cz.plitos.tgv_api.repository.CompanyRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CompanyServiceImpl extends CrudServiceImpl<Company, Long, CompanyRepository> implements CompanyService {
	public CompanyServiceImpl(CompanyRepository r) {
		super(r);
	}

	@Override
	public Optional<Company> readByName(String name) {
		return repo.findByName(name);
	}
}
