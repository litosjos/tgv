package cz.plitos.tgv_api.service;

import java.util.Optional;

public interface CrudService<E, ID> {
	public E create(E e);

	public Optional<E> readById(ID id);

	public Iterable<E> readAll();

	public void update(E e);

	public void deleteById(ID id);
}
