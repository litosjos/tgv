package cz.plitos.tgv_api.service;

import cz.plitos.tgv_api.domain.EntityWithId;
import org.springframework.data.repository.CrudRepository;

import java.util.NoSuchElementException;
import java.util.Optional;

public class CrudServiceImpl<E extends EntityWithId<ID>, ID, R extends CrudRepository<E, ID>> implements CrudService<E, ID> {
	protected final R repo;

	protected CrudServiceImpl(R repo) {
		this.repo = repo;
	}

	@Override
	public E create(E e) {
		if (e.getId() != null) throw new IllegalArgumentException("ID cannot be set during creation");
		return repo.save(e);
	}

	@Override
	public Optional<E> readById(ID id) {
		return repo.findById(id);
	}

	@Override
	public Iterable<E> readAll() {
		return repo.findAll();
	}

	public void update(E e) {
		if (!repo.existsById(e.getId())) throw new NoSuchElementException();
		repo.save(e);
	}

	@Override
	public void deleteById(ID id) {
		if (!repo.existsById(id)) throw new NoSuchElementException();
		try {
			repo.deleteById(id);
		} catch (Exception e) { // SQLIntegrityConstraintViolationException
			throw new IdUsedException();
		}
	}
}
