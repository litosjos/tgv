package cz.plitos.tgv_api.service;

import cz.plitos.tgv_api.domain.Train;

import java.util.Collection;
import java.util.Optional;

public interface TrainService extends CrudService<Train, Long> {
	public Collection<Train> readByCompanyOrStation(Optional<String> company,
																									Optional<String> station);
}
