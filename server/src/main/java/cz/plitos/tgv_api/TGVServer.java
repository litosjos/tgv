package cz.plitos.tgv_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TGVServer {
	public static void main(String[] args) {
		SpringApplication.run(TGVServer.class, args);
	}
}
