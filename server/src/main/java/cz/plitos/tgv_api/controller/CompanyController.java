package cz.plitos.tgv_api.controller;

import cz.plitos.tgv_api.domain.Company;
import cz.plitos.tgv_api.service.CompanyService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/company", produces = MediaType.APPLICATION_JSON_VALUE)
public class CompanyController extends CrudController<CompanyService, Company, Long> {
	public CompanyController(CompanyService s) {
		super(s);
	}

	@GetMapping
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "returned all instances or company with name",
			content = @Content),
		@ApiResponse(responseCode = "404", description = "no company with such name found")})
	@ResponseBody
	public Iterable<Company> readAll(@RequestParam Optional<String> name) {
		if (name.isEmpty()) return service.readAll();
		var got = service.readByName(name.get());
		if (got.isPresent()) return List.of(got.get());
		throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
}
