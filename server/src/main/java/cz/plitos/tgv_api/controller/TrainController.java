package cz.plitos.tgv_api.controller;

import cz.plitos.tgv_api.domain.Train;
import cz.plitos.tgv_api.service.TrainService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/train", produces = MediaType.APPLICATION_JSON_VALUE)
public class TrainController extends CrudController<TrainService, Train, Long> {
	public TrainController(TrainService s) {
		super(s);
	}

	@GetMapping
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "successfully returned all/filtered instances",
			content = @Content)})
	@ResponseBody
	public Iterable<Train> readAllOrByCompanyOrStation(@RequestParam Optional<String> company, @RequestParam Optional<String> station) {
		if (company.isEmpty() && station.isEmpty()) return service.readAll();
		else return service.readByCompanyOrStation(company, station);
	}
}
