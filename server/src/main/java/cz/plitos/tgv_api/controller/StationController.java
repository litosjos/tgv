package cz.plitos.tgv_api.controller;

import cz.plitos.tgv_api.domain.Station;
import cz.plitos.tgv_api.service.IdUsedException;
import cz.plitos.tgv_api.service.StationService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping(value = "/station", produces = MediaType.APPLICATION_JSON_VALUE)
public class StationController extends CrudController<StationService, Station, Long> {
	public StationController(StationService s) {
		super(s);
	}

	@GetMapping("/{id}/outgoing")
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "successfully returned all outgoing connections",
			content = @Content), @ApiResponse(responseCode = "404", description = "no such ID")})
	@ResponseBody
	public Iterable<Station> readOutgoing(@PathVariable Long id) {
		try {
			return service.readOutgoing(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/{id}/outgoing") // technically could use readAll with `outgoing` param
	@ApiResponses({@ApiResponse(responseCode = "204"),
		@ApiResponse(responseCode = "409", description = "destination references itself"),
		@ApiResponse(responseCode = "404", description = "no such ID")})
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void connect(@PathVariable Long id, @RequestBody Long dst) {
		try {
			service.connect(id, dst);
		} catch (IdUsedException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{id}/outgoing/{dst}")
	@ApiResponses({@ApiResponse(responseCode = "204"),
		@ApiResponse(responseCode = "404", description = "no such ID (- connection doesn't exist)")})
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void disconnect(@PathVariable Long id, @PathVariable Long dst) {
		try {
			service.disconnect(id, dst);
		} catch (
			NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "returned all instances or station with label",
			content = @Content),
		@ApiResponse(responseCode = "404", description = "no company with such name found")})
	@ResponseBody
	public Iterable<Station> readAll(@RequestParam Optional<String> label) {
		if (label.isEmpty()) return service.readAll();
		var got = service.readByLabel(label.get());
		if (got.isPresent()) return List.of(got.get());
		throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
}
