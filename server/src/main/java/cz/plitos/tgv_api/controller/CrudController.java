package cz.plitos.tgv_api.controller;

import cz.plitos.tgv_api.domain.EntityWithId;
import cz.plitos.tgv_api.service.CrudService;
import cz.plitos.tgv_api.service.IdUsedException;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.NoSuchElementException;

public class CrudController<S extends CrudService<E, ID>, E extends EntityWithId<ID>, ID> {
	protected final S service;

	protected CrudController(S service) {
		this.service = service;
	}

	@PostMapping
	@ApiResponses({@ApiResponse(responseCode = "200", content = @Content),
		@ApiResponse(responseCode = "409", description = "ID cannot be specified on creation")})
	@ResponseBody
	public E create(@RequestBody E data) {
		if (data.getId() != null)
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		return service.create(data);
	}

	@GetMapping("/{id}")
	@ApiResponses({@ApiResponse(responseCode = "200", content = @Content),
		@ApiResponse(responseCode = "404", description = "no such ID")})
	@ResponseBody
	public E readById(@PathVariable ID id) {
		var opt = service.readById(id);
		if (opt.isPresent()) return opt.get();
		else throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}

	@PutMapping("/{id}")
	@ApiResponses({@ApiResponse(responseCode = "204"),
		@ApiResponse(responseCode = "404", description = "no such ID")})
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void update(@PathVariable ID id, @RequestBody E data) {
		data.setId(id);
		try {
			service.update(data);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@ApiResponses({@ApiResponse(responseCode = "204"),
		@ApiResponse(responseCode = "404", description = "no such ID"),
		@ApiResponse(responseCode = "409", description = "ID is used by other object(s)")})
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable ID id) {
		try {
			service.deleteById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		} catch (IdUsedException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		}
	}
}
