#!/bin/bash
systemctl status postgresql | grep 'Active: active' || systemctl start postgresql
export SPRING_DATASOURCE_USERNAME=postgres
export SPRING_DATASOURCE_PASSWORD=
createdb -U $SPRING_DATASOURCE_USERNAME tgv
psql -U $SPRING_DATASOURCE_USERNAME -d tgv -f ./server/create.sql

export API_URL=http://localhost:8080
chmod a+x ./client/gradlew ./server/gradlew
{
	cd server && ./gradlew test bootJar && SPRING_PORT=${API_URL/*:/} java -jar build/libs/api*.jar &
}

export SPRING_PORT=9080
{
	cd client && ./gradlew bootJar && java -jar build/libs/client*.jar &
}

sleep 5 && $BROWSER "http://localhost:$SPRING_PORT/"
